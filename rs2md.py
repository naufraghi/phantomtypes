#!/bin/env python3

import argparse
import re


TOP_DOCS = re.compile(f"^//! ?")


def get_in_code_matcher():
    inside = True
    while True:
        yield inside
        inside = not inside


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("source", type=argparse.FileType())

    args = parser.parse_args()

    in_code_matcher = get_in_code_matcher()

    in_code = False

    for line in args.source:
        if TOP_DOCS.match(line):
            line = TOP_DOCS.sub("", line)
            if line.startswith("```"):
                in_code = next(in_code_matcher)
            if in_code and line.startswith("#"):
                continue
            if not in_code:
                if line.endswith("\n") and line != "\n":
                    line = line.replace("\n", " ")
                if line == "\n":
                    line = "\n\n"
            if in_code:
                line = line.replace("rust,ignore", "rust")
            print(line, end="")


if __name__ == "__main__":
    main()
