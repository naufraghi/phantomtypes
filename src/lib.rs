extern crate chrono;

mod part1;
mod part2;

use chrono::prelude::*;

use std::marker::PhantomData;

trait Now {
    fn now() -> DateTime<Utc> {
        Utc::now()
    }
}

#[derive(Debug)]
struct SystemNow;
impl Now for SystemNow {}

#[derive(Debug)]
struct Event<T: Now = SystemNow> {
    message: String,
    stamp: DateTime<Utc>,
    mark: PhantomData<T>,
}

impl<T: Now> Event<T> {
    fn new(message: String) -> Event<T> {
        Event {
            message,
            stamp: T::now(),
            mark: PhantomData,
        }
    }
}

type SystemEvent = Event<SystemNow>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let _event = SystemEvent::new("SystemEvent".to_owned());
        //assert_eq!(_event.stamp.year(), 2018);  // will fail next year
        //assert_eq!(_event.stamp.minute(), 43);  // may fail 59 over 60 minutes
    }

    #[test]
    fn test_new_fixed() {
        #[derive(Debug)]
        struct FixedNow;

        impl Now for FixedNow {
            fn now() -> DateTime<Utc> {
                Utc.ymd(2001, 9, 9).and_hms(1, 46, 40)
            }
        }

        type FixedEvent = Event<FixedNow>;

        let event = FixedEvent::new("FixedEvent".to_owned());
        assert_eq!(event.stamp.year(), 2001);
        assert_eq!(event.stamp.minute(), 46);
    }

    #[test]
    fn test_loop_unsafe_new() {
        #[derive(Debug)]
        struct LoopNow;

        impl Now for LoopNow {
            fn now() -> DateTime<Utc> {
                static mut COUNTER: i32 = 0;
                let inc = unsafe {
                    COUNTER = (COUNTER + 1) % 10;
                    COUNTER
                };
                Utc.ymd(2000 + inc, 9, 9).and_hms(1, 46, 40)
            }
        }
        type LoopEvent = Event<LoopNow>;

        let event1 = LoopEvent::new("LoopEvent 1".to_owned());
        assert_eq!(event1.stamp.year(), 2001);
        assert_eq!(event1.stamp.minute(), 46);

        let event2 = LoopEvent::new("LoopEvent 2".to_owned());
        assert_eq!(event2.stamp.year(), 2002);
        assert_eq!(event2.stamp.minute(), 46);
    }

    #[test]
    fn test_atomic_increment_new() {
        #[derive(Debug)]
        struct IncrementNow;

        impl Now for IncrementNow {
            fn now() -> DateTime<Utc> {
                use std::sync::atomic::{AtomicUsize, Ordering};
                static COUNTER: AtomicUsize = AtomicUsize::new(1);
                let inc = COUNTER.fetch_add(1, Ordering::SeqCst) as i32;
                Utc.ymd(2000 + inc, 9, 9).and_hms(1, 46, 40)
            }
        }
        type IncrementEvent = Event<IncrementNow>;

        let event1 = IncrementEvent::new("IncrementEvent 1".to_owned());
        assert_eq!(event1.stamp.year(), 2001);
        assert_eq!(event1.stamp.minute(), 46);

        let event2 = IncrementEvent::new("IncrementEvent 2".to_owned());
        assert_eq!(event2.stamp.year(), 2002);
        assert_eq!(event2.stamp.minute(), 46);
    }
}
